from django.apps import AppConfig


class RegisterDonaturConfig(AppConfig):
    name = 'register_donatur'
