from django.conf.urls import url
from .views import index, yayasan, individu
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'yayasan/', yayasan, name='yayasan'),
    url(r'individu/', individu, name='individu'),


]