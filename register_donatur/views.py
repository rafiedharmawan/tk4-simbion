from django.shortcuts import render

# Create your views here.

def index(request):
    response = {}
    return render(request, 'registerdnr_landing.html', response)

def yayasan(request):
    response = {}
    return render(request, 'yayasan_landing.html', response)

def individu(request):
    response = {}
    return render(request, 'individu_landing.html', response)